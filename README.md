# Bootcamp Arkademy Submission

**English** | [Bahasa Indonesia](README_id.md)

A set of programs that were written and submitted for **Bootcamp Arkademy Batch 12** selection.

## List of Contents

* [Bootcamp Arkademy Submission](#bootcamp-arkademy-submission)
  * [List of Contents](#list-of-contents)
  * [System Requirement](#system-requirements)
  * [Getting Started](#getting-started)
  * [Documentation](#documentation)
    * [Solution toward Problem 1 to 5](#solution-toward-problem-1-to-5)
    * [Solution towards Problem 6](#solution-towards-problem-6)
  * [Built With](#built-with)
  * [Contribution](#contribution)
  * [Authors](#authors)
  * [License](#license)

## System Requirements

This program **requires these softwares** below to be installed in your system.

 * [Git](https://git-scm.com)
 * [NodeJS](https://nodejs.org)
 
Before continuing, please make sure the mentioned softwares is installed.

## Getting Started

To start, please **clone this repository**.

```bash
git clone https://gitlab.com/danang-id/bootcamp-arkademy-submission.git
cd bootcamp-arkademy-submission
```

## Documentation

How to test the solutions describe below.

### Solution toward Problem 1 to 5

For solution toward problem 1 to 5, you must **import them** in another JavaScript file, and then call the available function from the within. For example, create `app.js` file with the following content to run the solution of problem 1.

```javascript
const solution = require("./solution-1");
console.log(solution.getBioData());
// Will then print the bio data
```

Or to make thing simpler, you may **use the [ES6 Console](https://es6console.com/) and run the file online**.

Here are the **list of available functions** on each solution.

 | Solution of | Available Functions | ES6 Console Link |
 | ----------- | ------------------- | ---------------- |
 | Problem 1   | **getBioData**() | [https://es6console.com/k0jd1crr](https://es6console.com/k0jd1crr) |
 | Problem 2   | **isUsernameValid**(username: _string_) | [https://es6console.com/k0jd22a6](https://es6console.com/k0jd22a6) |
 | Problem 2   | **isPasswordValid**(password: _string_) | [https://es6console.com/k0jd22a6](https://es6console.com/k0jd22a6) |
 | Problem 3   | **countPhrase**(string: _string_, phrase: _string_) | [https://es6console.com/k0jd2pi8](https://es6console.com/k0jd2pi8) |
 | Problem 4   | **printDrawing**(size: _number_) | [https://es6console.com/k0jd3dc3](https://es6console.com/k0jd3dc3) |
 | Problem 5   | **printRandomStrings**(n: _number_) | [https://es6console.com/k0jd3w2y](https://es6console.com/k0jd3w2y) |
 
### Solution towards Problem 6

Problem 6 is to create a project with a set of blueprint. By the author, this project then divided into 2 parts -- server side and client side --, but it cannot run separately.

To see the project live on run, first **build the project using NPM** (included in NodeJS installation).

```bash
cd solution-6
npm run build
```

The build process may take a while, depending on your computer performance. This command will build both the server and client program.

After the build process has been finished, **start the program**.

```bash
npm start
```

Unless defined in the `.env` file, the program should run on localhost HTTP port 9000. So, open up [http://localhost:9000](http://localhost:9000) to see the result.

Here are some **screen captures with key point** of problem 6.

**6.A. Database Create Query**:

[![Database Create Query](screencaptures/screencapture-database-create.png)](screencaptures/screencapture-database-create.png)

**6.A. Database Select Query**:

[![Database Select Query](screencaptures/screencapture-database-select.png)](screencaptures/screencapture-database-select.png)

**6.B. Static Data**:

[![Static Data](screencaptures/screencapture-static.png)](screencaptures/screencapture-static.png)

**6.C. Dynamic Data - Get Persons**:

[![Dynamic Data - Get Persons](screencaptures/screencapture-dynamic-get-persons.png)](screencaptures/screencapture-dynamic-get-persons.png)

**6.C. Dynamic Data - Add Person**:

[![Dynamic Data - Add Person](screencaptures/screencapture-dynamic-add-person.png)](screencaptures/screencapture-dynamic-add-person.png)

**6.C. Dynamic Data - Modify Person**:

[![Dynamic Data - Modify Person](screencaptures/screencapture-dynamic-modify-person.png)](screencaptures/screencapture-dynamic-modify-person.png)

**6.C. Dynamic Data - Delete Person**:

[![Dynamic Data - Delete Person](screencaptures/screencapture-dynamic-delete-person.png)](screencaptures/screencapture-dynamic-delete-person.png)

**6.C. Dynamic Data - Add Work**:

[![Dynamic Data - Add Work](screencaptures/screencapture-dynamic-add-work.png)](screencaptures/screencapture-dynamic-add-work.png)

**6.C. Dynamic Data - Add Salary**:

[![Dynamic Data - Add Salary](screencaptures/screencapture-dynamic-add-salary.png)](screencaptures/screencapture-dynamic-add-salary.png)

## Built With

Solution towards problem 1 to 5 written in **JavaScript (CommonJS)**. Solution toward problem 6 written in different language described below.

 * Server side: **Express** with [TypeScript](https://typscriptlang.org/), built into ECMAScript 5 using the TypeScript and Babel compiler.
 * Client side: **React** with ECMAScript 6, build into JavaScript UMD using the Babel compiler.
 
## Contribution

Because this program is created for Bootcamp Arkademy Batch 12 selection submission, therefore **no contribution will be accepted**.

## Authors

Written and maintained by **Danang Galuh Tegar Prasetyo** - [danang-id](https://github.com/danang-id). 

## License

This project is licensed under the **Apache 2.0 License** - see the [LICENSE](LICENSE) file for details.

