/**
 * Solution of Problem 2
 * Date     : September 14, 2019
 * Problem  : Create functions which validate username and password.
 *
 * This program was written and submitted for Bootcamp Arkademy
 * Batch 12 selection.
 *
 * Copyright 2019, Danang Galuh Tegar Prasetyo.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * isUsernameValid()
 * This function is used validate a given username string according to these rules.
 *  1. Length between 5-9 characters.
 *  2. Consists of alphabetical (lowercase or uppercase) or numerical character.
 *  3. Must not begin with numerical or special character.
 *
 * @param { string } username Username to be validated.
 * @return { boolean }  Indicating whether the given username is valid.
 * @example
 *
 * isUsernameValid("Ayu99v")
 * // => true
 * isUsernameValid("@sony")
 * // => false
 */
function isUsernameValid(username) {
	// If username param is undefined, return false
	if (username === void 0) {
		return false;
	}
	// Declare and define regular expression
	// Doc: https://regexr.com/4l092
	const regularExpression = new RegExp(/^(?=.{5,9}$)(?=.*[a-zA-Z0-9]$)[^0-9!@#$%^&*]/);
	// Run a regex test against the username
	// and return the result
	return regularExpression.test(username);
}

/**
 * isPasswordValid()
 * This function is used validate a given password string according to these rules.
 *  1. Length minimum of 8 characters.
 *  2. Consists of minimum 1 lowercase or uppercase alphabetical character.
 *  3. Consists of minimum 1 numeric character.
 *  4. Consists of minimum 1 special character.
 *  5. Contains @ character.
 *
 * @param { string } password Password to be validated.
 * @return { boolean } Indicating whether the given password is valid.
 * @example
 *
 * isPasswordValid("p@ssW0rd#")
 * // => true
 * isPasswordValid("C0d3YourFuture!#")
 * // => false
 */
function isPasswordValid(password) {
	// If password param is undefined, return false
	if (password === void 0) {
		return false;
	}
	// Declare and define regular expression
	// Doc: https://regexr.com/4l095
	const regularExpression = new RegExp(/^(?=.{8,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[@])/);
	// Run a regex test against the password
	// and return the result
	return regularExpression.test(password);

}

module.exports = { isUsernameValid, isPasswordValid };
