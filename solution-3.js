/**
 * Solution of Problem 3
 * Date     : September 14, 2019
 * Problem  : Create a function which count how many times a phrase appears in a string,
 *            both from left to right and right to left direction.
 *
 * This program was written and submitted for Bootcamp Arkademy
 * Batch 12 selection.
 *
 * Copyright 2019, Danang Galuh Tegar Prasetyo.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * countPhrase()
 * This function will count how many times a phrase appears in a string, both from left
 * to right and right to left direction.
 *
 * @param { string } string The string to be looked for.
 * @param { string } phrase The phrase to look in the string.
 * @return { void | number } How many times the phrase appears.
 * @example
 *
 * countPhrase("banananana", "nana")
 * // => Found 6 times.
 *
 * countPhrase("bnananana", "nana")
 * // => Found 5 times.
 */
function countPhrase(string, phrase) {
	// Internal function to check whether a given variable is a number
	function isString(val) {
		return typeof val == "string" || (
			typeof val == "object" &&
			Object.prototype.toString.call(val) === "[object String]"
		);
	}
	// Internal recursive function to count how many times a phrase appears in a string
	function count(_string, _count = 0) {
		if (_string.length > phrase.length) {
			const subString = _string.substr(0, phrase.length);
			_count = subString === phrase ? (_count + 1) : _count;
			return count(_string.substr(1), _count);
		} else if (_string.length === phrase.length) {
			return _string === phrase ? (_count + 1) : _count;
		} else {
			return _count;
		}
	}
	// String param must be defined
	if (string === void 0) {
		console.log("Please provide \"string\" parameter!");
		return;
	}
	// Phrase param must be defined
	if (phrase === void 0) {
		console.log("Please provide \"phrase\" parameter!");
		return;
	}
	// String param must be a string
	if (!isString(string)) {
		console.log("Parameter \"string\" must be a string!");
		console.log("Provided Type: " + typeof string);
		return;
	}
	// Phrase param must be a string
	if (!isString(phrase)) {
		console.log("Parameter \"phrase\" must be a string!");
		console.log("Provided Type: " + typeof phrase);
		return;
	}
	// Phrase param not be longer than string param
	if (phrase.length > string.length) {
		console.log("Parameter \"phrase\" must not be longer than parameter \"string\"!");
		console.log("Provided \"string\" : " + string);
		console.log("Provided \"phrase\" : " + phrase);
		return;
	}
	// Count left to right
	const ltrCount = count(string);
	// Count right to left
	const rtlString = string.split("").reverse().join("");
	const rtlCount = count(rtlString);
	// Return and print total count
	console.log("Found " + (ltrCount + rtlCount) + " times.");
	return ltrCount + rtlCount;
}

module.exports = { countPhrase };
