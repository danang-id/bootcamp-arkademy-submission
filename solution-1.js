/**
 * Solution of Problem 1
 * Date     : September 14, 2019
 * Problem  : Create a function which returns my bio data.
 *
 * This program was written and submitted for Bootcamp Arkademy
 * Batch 12 selection.
 *
 * Copyright 2019, Danang Galuh Tegar Prasetyo.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * getBioData()
 * This function will returns my bio data.
 *
 * @return { string } Prettified JSON string containing my bio data. *
 * @example
 *
 * getBioData()
 */
function getBioData() {
    // Declare and define bio data object
    const bioData = {
        name: "Danang Galuh Tegar Prasetyo",
        age: 24,
        address: "Kedokan RT/RW 073/034 Sidoharjo, Samigaluh, Kulon Progo 55673 D.I. Yogyakarta",
        hobbies: [
            "programming", "gaming", "eat"
        ],
        is_married: false,
        list_school: [
            {
                name: "SD N Samigaluh 1",
                year_in: 2001,
                year_out: 2007,
                major: null
            },
            {
                name: "SMP N 1 Samigaluh",
                year_in: 2007,
                year_out: 2010,
                major: null
            },
            {
                name: "SMA N 1 Wates",
                year_in: 2010,
                year_out: 2013,
                major: "Science"
            }
        ],
        skills: [
            {
                skill_name: "JavaScript",
                level: "advanced"
            }
        ],
        interest_in_coding: true
    };
    // Convert bio data object to prettified JSON string
    // and returns it
    return JSON.stringify(bioData, null, 4);
}

module.exports = { getBioData };
