/**
 * Solution of Problem 5
 * Date     : September 14, 2019
 * Problem  : Create a function which print n different random strings
 *            with 32 characters length, and which n is provided in parameter.
 *
 * This program was written and submitted for Bootcamp Arkademy
 * Batch 12 selection.
 *
 * Copyright 2019, Danang Galuh Tegar Prasetyo.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * printRandomStrings()
 * This function will print n different random strings, with 32 characters
 * length, and which n is provided in parameter.
 *
 * @param { number } n How many random strings should be printed.
 * @return { void } A print of the random strings.
 * @example
 *
 * printRandomStrings(3)
 * // => da2c312dfe804ef5bd318133a342251a
 * //    79n89a9mdfe804ef5b18133a342251o
 * //    6e576057da174c4189f7ea8341946aed
 */
function printRandomStrings(n) {
	// Internal function to check whether a given variable is a number
	function isNumber(val) {
		return typeof val == "number" || (
			typeof val == "object" &&
			Object.prototype.toString.call(val) === "[object Number]"
		);
	}
	// Internal function to generate 32 characters length random string
	function generateRandomString() {
		const usedCharacters = "abcdefghijklmnopqrstuvwxyz0123456789";
		let random = "";
		for (let i = 0; i < 32; i++ ) {
			random += usedCharacters.charAt(Math.floor(Math.random() * usedCharacters.length));
		}
		return random;
	}
	// n param must be defined
	if (n === void 0) {
		console.log("Please provide \"n\" parameter!");
		return;
	}
	// n param must be a number
	if (!isNumber(n)) {
		console.log("Parameter \"n\" must be a string!");
		console.log("Provided Type: " + typeof n);
		return;
	}
	// n param must be a positive number
	if (n < 1) {
		console.log("Parameter \"n\" must be a positive number!");
		console.log("Provided Value: " + n);
		return;
	}
	// Generate n different random strings
	const randomStrings = [];
	for (let i = 0; i < n; i++) {
		// Get a random string
		let randomString = generateRandomString();
		// Check if this random string have ever been generated and pushed to the stack
		let foundSameRandom = randomStrings.findIndex(string => string === randomString) !== -1;
		// If this random string have ever been generated, regenerate a new random string
		// until that random string is found to be not exist in the stack (and does mean
		// never been generated before
		while (foundSameRandom) {
			randomString = generateRandomString();
			foundSameRandom = randomStrings.findIndex(string => string === randomString) !== -1;
		}
		// Push the new random string to the stack
		randomStrings.push(randomString);
	}
	console.log(randomStrings.join("\n"));
}

module.exports = { printRandomStrings };
