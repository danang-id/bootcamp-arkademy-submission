/**
 * Solution of Problem 4
 * Date     : September 14, 2019
 * Problem  : Create a function which print a set of drawing which size defined
 *            in the parameter.
 *
 * This program was written and submitted for Bootcamp Arkademy
 * Batch 12 selection.
 *
 * Copyright 2019, Danang Galuh Tegar Prasetyo.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * printDrawing()
 * This function will print a set of drawing which size defined in the parameter.
 *
 * @param { number } size The positive odd number indicating drawing size (minimum 3).
 * @return { void } A print of the drawing.
 * @example
 *
 * printDrawing(3)
 * // => * = *
 * //    * * *
 * //    * = *
 *
 * printDrawing(5)
 * // => * = = = *
 * //    * = = = *
 * //    * * * * *
 * //    * = = = *
 * //    * = = = *
 */
function printDrawing(size) {
	// Internal function to check whether a given variable is a number
	function isNumber(val) {
		return typeof val == "number" || (
			typeof val == "object" &&
			Object.prototype.toString.call(val) === "[object Number]"
		);
	}
	// Internal recursive function to draw a single characters n times,
	// separated by separation character
	function draw(char, n, separatedBy = "") {
		return n > 1 ? char + separatedBy + draw(char, n - 1, separatedBy) : char + separatedBy;
	}
	// Size param must be defined
	if (size === void 0) {
		console.log("Please provide \"size\" parameter!");
		return;
	}
	// Size param must be a number
	if (!isNumber(size)) {
		console.log("Parameter \"size\" must be a number!");
		console.log("Provided Type: " + typeof size);
		return;
	}
	// Size param must be an odd number
	if (size % 2 !== 1) {
		console.log("Parameter \"size\" must be an odd number!");
		console.log("Provided Value: " + size);
		return;
	}
	// Size param must be minimal of 3
	if (size < 3) {
		console.log("Parameter \"size\" must be a minimal of 3!");
		console.log("Provided Value: " + size);
		return;
	}
	const centerIndex = Math.floor(size / 2);
	let drawing = "";
	for (let index = 0; index < size; index++) {
		if (index !== centerIndex) {
			drawing += draw("*", 1, " ");
			drawing += draw("=", size - 2, " ");
			drawing += draw("*", 1, " ");
		} else {
			drawing += draw("*", size, " ");
		}
		drawing += "\n";
	}
	console.log(drawing);
}

module.exports = { printDrawing };
