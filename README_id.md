# Bootcamp Arkademy Submission

[English](README.md) | **Bahasa Indonesia**

Sebuah kumpulan program yang ditulis dan diajukan untuk seleksi **Bootcamp Arkademy Batch 12**.

## Daftar Isi

* [Bootcamp Arkademy Submission](#bootcamp-arkademy-submission)
  * [Daftar Isi](#daftar-isi)
  * [Kebutuhan Sistem](#kebutuhan-sistem)
  * [Memulai](#memulai)
  * [Dokumentasi](#dokumentasi)
    * [Solusi dari Soal 1 sampai 5](#solusi-dari-soal-1-sampai-5)
    * [Solusi dari Soal 6](#solusi-dari-soal-6)
  * [Dibangun dengan](#dibangun-dengan)
  * [Kontribusi](#kontribusi)
  * [Penulis](#penulis)
  * [Lisensi](#lisensi)

## Kebutuhan Sistem

Program ini **membutuhkan perangkat lunak di bawah** terpasang di sistem Anda.

 * [Git](https://git-scm.com)
 * [NodeJS](https://nodejs.org)
 
Sebelum melanjutkan, pastikan perangkat lunak tersebut telah terpasang.

## Memulai

Untuk memulai, silakan **_clone_ repositori ini**.

```bash
git clone https://gitlab.com/danang-id/bootcamp-arkademy-submission.git
cd bootcamp-arkademy-submission
```

## Dokumentasi

Cara untuk menguji solusi-solusi dijelaskan di bawah.

### Solusi dari Soal 1 sampai 5

Untuk solusi dari soal 1 sampai 5, Anda harus **mengimpornya** di berkas JavaScript lain, dan kemudian memanggil fungsi yang tersedia dari dalam. Sebagai contoh, buatlah berkas `app.js` dengan isi berikut untuk menjalakan solusi dari soal 1.

```javascript
const solution = require("./solution-1");
console.log(solution.getBioData());
// Akan mencentak bio data
```

Atau untuk membuatnya lebih sederhana, Anda dapat **menggunakan [ES6 Console](https://es6console.com/) dan jalankan berkas secara online**.

Berikut adalah **daftar fungsi yang tersedia** dari setiap solusi.

 | Solusi dari | Fungsi Tersedia | ES6 Console Link |
 | ----------- | --------------- | ---------------- |
 | Soal 1   | **getBioData**() | [https://es6console.com/k0jd1crr](https://es6console.com/k0jd1crr) |
 | Soal 2   | **isUsernameValid**(username: _string_) | [https://es6console.com/k0jd22a6](https://es6console.com/k0jd22a6) |
 | Soal 2   | **isPasswordValid**(password: _string_) | [https://es6console.com/k0jd22a6](https://es6console.com/k0jd22a6) |
 | Soal 3   | **countPhrase**(string: _string_, phrase: _string_) | [https://es6console.com/k0jd2pi8](https://es6console.com/k0jd2pi8) |
 | Soal 4   | **printDrawing**(size: _number_) | [https://es6console.com/k0jd3dc3](https://es6console.com/k0jd3dc3) |
 | Soal 5   | **printRandomStrings**(n: _number_) | [https://es6console.com/k0jd3w2y](https://es6console.com/k0jd3w2y) |
 
### Solusi dari Soal 6

Soal 6 adalah untuk membuat sebuah proyek dengan satu set cetak biru. Oleh penulis, proyek ini kemudian dibagi menjadi 2 bagian -- sisi _server_ dan sisi _client_ --, tetapi keduanya tidak dapat berjalan secara terpisah.

Untuk melihat proyek berjalan secara langsung, pertama **_build_ (bangun) proyek menggunakan NPM** (bawaan ketika memasang NodeJS). 

```bash
cd solution-6
npm run build
```

Proses pembangunan akan memakan beberapa waktu, bergantung pada peforma komputer. Perintah ini akan membangun baik program _server_ dan _client_.

Setelah proses pembangunan selesai, **mulai program**.

```bash
npm start
```

Kecuali didefinisikan pada berkas `.env`, program akan berjalan pada localhost HTTP port 9000. Jadi, silakan buka [http://localhost:9000](http://localhost:9000) untuk melihat hasil.

Berikut beberapa **tangkapan layar dengan poin kunci** dari soal 6.

**6.A. Database Create Query**:

[![Database Create Query](screencaptures/screencapture-database-create.png)](screencaptures/screencapture-database-create.png)

**6.A. Database Select Query**:

[![Database Select Query](screencaptures/screencapture-database-select.png)](screencaptures/screencapture-database-select.png)

**6.B. Static Data**:

[![Static Data](screencaptures/screencapture-static.png)](screencaptures/screencapture-static.png)

**6.C. Dynamic Data - Get Persons**:

[![Dynamic Data - Get Persons](screencaptures/screencapture-dynamic-get-persons.png)](screencaptures/screencapture-dynamic-get-persons.png)

**6.C. Dynamic Data - Add Person**:

[![Dynamic Data - Add Person](screencaptures/screencapture-dynamic-add-person.png)](screencaptures/screencapture-dynamic-add-person.png)

**6.C. Dynamic Data - Modify Person**:

[![Dynamic Data - Modify Person](screencaptures/screencapture-dynamic-modify-person.png)](screencaptures/screencapture-dynamic-modify-person.png)

**6.C. Dynamic Data - Delete Person**:

[![Dynamic Data - Delete Person](screencaptures/screencapture-dynamic-delete-person.png)](screencaptures/screencapture-dynamic-delete-person.png)

**6.C. Dynamic Data - Add Work**:

[![Dynamic Data - Add Work](screencaptures/screencapture-dynamic-add-work.png)](screencaptures/screencapture-dynamic-add-work.png)

**6.C. Dynamic Data - Add Salary**:

[![Dynamic Data - Add Salary](screencaptures/screencapture-dynamic-add-salary.png)](screencaptures/screencapture-dynamic-add-salary.png)

## Dibangun dengan

Solusi dari soal 1 sampai 5 ditulis dengan **JavaScript (CommonJS)**. Solusi dari Soal 6 ditulis dengan bahasa yang berbeda sesuai dideskripsikan di bawah.

 * Sisi _server_: **Express** dengan [TypeScript](https://typscriptlang.org/), dibangun ke ECMAScript 5 menggunakan TypeScript dan Babel compiler.
 * Sisi _client_: **React** dengan ECMAScript 6, dibangun ke JavaScript UMD menggunakan Babel compiler.
 
## Kontribusi

Karena program ini diciptakan untuk diajukan pada seleksi Bootcamp Arkademy Batch 12, maka dari itu **tidak akan ada kontribusi yang akan diterima**.

## Penulis

Ditulis dan dikelola oleh **Danang Galuh Tegar Prasetyo** - [danang-id](https://github.com/danang-id). 

## Lisensi

Proyek ini dilisensikan di bawah **Lisensi Apache 2.0** - silakan lihat berkas [LICENSE](LICENSE) untuk detil.

