/**
 * Solution of Problem 6
 * Date     : September 14, 2019
 * Problem  : Create a project.
 *
 * This program was written and submitted for Bootcamp Arkademy
 * Batch 12 selection.
 *
 * Copyright 2019, Danang Galuh Tegar Prasetyo.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from "react";
import LoadingBar from "react-top-loading-bar";
import { ToastContainer, toast } from "react-toastify";
import NavigationBar from "./components/NavigationBar";
import Footer from "./components/Footer";
import Dashboard from "./views/Dashboard";

export class App extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			usingDynamicData: false,
			loadingProgress: 0
		};
	}

	componentDidMount = () => {
		window.addEventListener("online", this.onOnline);
		window.addEventListener("offline", this.onOffline);
	};

	componentWillUnmount = () => {
		window.removeEventListener("online", this.onOnline);
		window.removeEventListener("offline", this.onOffline);
	};

	onOnline = () => {
		this.onShowToast("success", "You are back online! ☺ ");
	};

	onOffline = () => {
		this.onShowToast("error", "You are offline. Please make sure you are connected to the network. ☹");
	};

	onChangeDataSource = (usingDynamicData) => {
		if (this.state.loadingProgress === 0) {
			this.setState({
				...this.state,
				...{
					usingDynamicData
				}
			});
		} else {
			this.onShowToast(
				"warning",
				"Failed to switch to static data source",
				"An operation is currently in progress. Please wait until current operation complete before switching data source."
			);
		}
	};

	onAddLoadingProgress = (addPercentage) => {
		this.setState({
			...this.state,
			...{
				loadingProgress: typeof addPercentage !== "undefined" ? this.state.loadingProgress + addPercentage : 100
			}
		});
	};

	onLoaderFinished = () => {
		this.setState({
			...this.state,
			...{
				loadingProgress: 0
			}
		});
	};

	onShowToast = (type, title, message) => {
		const options = {
			position: "bottom-right",
			autoClose: 5000,
			hideProgressBar: true,
			closeOnClick: true,
			pauseOnHover: true,
			draggable: true,
		};
		if (typeof message === "undefined") {
			message = title;
			title = null;
		}
		const content = (
			<div>
				<strong>{ title }</strong>{ !!title ? (<br />) : null }{ message }
			</div>
		);
		toast[type](content, options);
	};

	render() {
		return (
			<div className="App">
				<LoadingBar
					color="orange"
					progress={ this.state.loadingProgress }
					onLoaderFinished={ this.onLoaderFinished }
				/>
				<NavigationBar
					usingDynamicData={ this.state.usingDynamicData }
					onChangeDataSource={ this.onChangeDataSource }
				/>
				<Dashboard
					usingDynamicData={ this.state.usingDynamicData }
					readyState={ this.state.loadingProgress === 0 }
					onAddLoadingProgress={ this.onAddLoadingProgress }
					onShowToast={ this.onShowToast }
				/>
				<Footer />
				<ToastContainer
					position="top-right"
					autoClose={ 5000 }
					hideProgressBar
					newestOnTop={false}
					closeOnClick
					pauseOnVisibilityChange
					draggable
					pauseOnHover
				/>
			</div>
		)
	}
}

export default App;
