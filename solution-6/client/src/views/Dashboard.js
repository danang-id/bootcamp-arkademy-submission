/**
 * Solution of Problem 6
 * Date     : September 14, 2019
 * Problem  : Create a project.
 *
 * This program was written and submitted for Bootcamp Arkademy
 * Batch 12 selection.
 *
 * Copyright 2019, Danang Galuh Tegar Prasetyo.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import ButtonToolbar from "react-bootstrap/ButtonToolbar";
import Button from "react-bootstrap/Button";
import Table from "react-bootstrap/Table";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";
import { getPersons, createPerson, modifyPerson, deletePerson } from "../services/PersonAPI";
import { getSalaries, createSalary } from "../services/SalaryAPI";
import { getWorks, createWork } from "../services/WorkAPI";
import "./Dashboard.css";

export default class Dashboard extends React.Component {
	constructor(props) {
		super(props);
		this.staticData = [
			{
				id: null,
				id_salary: null,
				id_work: null,
				name: "Rebecca",
				work: "Frontend Dev",
				salary: "10000000",
			},
			{
				id: null,
				id_salary: null,
				id_work: null,
				name: "Vita",
				work: "Backend Dev",
				salary: "12000000",
			}
		];
		this.state = {
			persons: this.staticData,
			salaries: [],
			works: [],
			selectedPerson: {},
			selectedSalary: {},
			selectedWork: {},
			createPersonModalShown: false,
			modifyPersonModalShown: false,
			deletePersonModalShown: false,
			createSalaryModalShown: false,
			createWorkModalShown: false,
		};
	}

	componentWillReceiveProps = nextProps => {
		if (nextProps.usingDynamicData !== this.props.usingDynamicData) {
			if (nextProps.usingDynamicData) {
				this.getPersons().then();
			} else {
				this.onPersonsChange(this.staticData);
			}
		}
	};

	getPersons = async () => {
		try {
			this.props.onAddLoadingProgress(20);
			const response = await getPersons();
			setTimeout(() => {
				this.onPersonsChange(response.data);
			}, 0);
		} catch (error) {
			this.onPersonsChange([]);
			this.props.onShowToast("error", "Failed to get persons data", error.message);
		} finally {
			this.props.onAddLoadingProgress();
		}
	};

	createPerson = async () => {
		if (!this.state.selectedPerson.name || this.state.selectedPerson.name.trim() === "") {
			this.props.onShowToast("error", "Failed to add person data", "Person name cannot be empty.");
			return;
		}
		if (!this.state.selectedPerson.id_work || this.state.selectedPerson.id_work === -1) {
			this.props.onShowToast("error", "Failed to add person data", "Please choose a work.");
			return;
		}
		if (!this.state.selectedPerson.id_salary || this.state.selectedPerson.id_salary === -1) {
			this.props.onShowToast("error", "Failed to add person data", "Please choose a salary.");
			return;
		}
		try {
			this.props.onAddLoadingProgress(20);
			const response = await createPerson(
				this.state.selectedPerson.name.trim(),
				this.state.selectedPerson.id_salary,
				this.state.selectedPerson.id_work,
			);
			this.onHideCreatePersonModal();
			await this.getPersons();
			this.props.onShowToast("success", "Success!", response.message);
		} catch (error) {
			this.props.onShowToast("error", "Failed to create person data", error.message);
		} finally {
			this.props.onAddLoadingProgress();
		}
	};

	modifyPerson = async () => {
		if (!this.state.selectedPerson.id) {
			this.props.onShowToast("error", "Failed to modify person data", "Person identifier is not defined.");
			return;
		}
		if (!this.state.selectedPerson.name || this.state.selectedPerson.name.trim() === "") {
			this.props.onShowToast("error", "Failed to modify person data", "Person name cannot be empty.");
			return;
		}
		if (!this.state.selectedPerson.id_work || this.state.selectedPerson.id_work === -1) {
			this.props.onShowToast("error", "Failed to modify person data", "Please choose a work.");
			return;
		}
		if (!this.state.selectedPerson.id_salary || this.state.selectedPerson.id_salary === -1) {
			this.props.onShowToast("error", "Failed to modify person data", "Please choose a salary.");
			return;
		}
		try {
			this.props.onAddLoadingProgress(20);
			const response = await modifyPerson(
				this.state.selectedPerson.id,
				this.state.selectedPerson.name.trim(),
				this.state.selectedPerson.id_salary,
				this.state.selectedPerson.id_work,
			);
			this.onHideModifyPersonModal();
			await this.getPersons();
			this.props.onShowToast("success", "Success!", response.message);
		} catch (error) {
			this.props.onShowToast("error", "Failed to modify person data", error.message);
		} finally {
			this.props.onAddLoadingProgress();
		}
	};

	deletePerson = async () => {
		if (!this.state.selectedPerson.id) {
			this.props.onShowToast("error", "Failed to delete person data", "Person identifier is not defined.");
			return;
		}
		try {
			this.props.onAddLoadingProgress(20);
			const response = await deletePerson(this.state.selectedPerson.id);
			this.onHideDeletePersonModal();
			await this.getPersons();
			this.props.onShowToast("success", "Success!", response.message);
		} catch (error) {
			this.props.onShowToast("error", "Failed to delete person data", error.message);
		} finally {
			this.props.onAddLoadingProgress();
		}
	};

	getSalaries = async () => {
		try {
			this.props.onAddLoadingProgress(20);
			const response = await getSalaries();
			this.onSalariesChange(response.data);
		} catch (error) {
			this.onSalariesChange([]);
			this.props.onShowToast("error", "Failed to get salaries data", error.message);
		} finally {
			this.props.onAddLoadingProgress();
		}
	};

	createSalary = async () => {
		if (!this.state.selectedSalary.salary || parseInt(this.state.selectedSalary.salary) <= 0) {
			this.props.onShowToast(
				"error",
				"Failed to add salary data",
				"Salary value must be positive number (more than 0).",
			);
			return;
		}
		try {
			this.props.onAddLoadingProgress(20);
			const response = await createSalary(this.state.selectedSalary.salary);
			this.onHideCreateSalaryModal();
			await getSalaries();
			this.props.onShowToast("success", "Success!", response.message);
		} catch (error) {
			this.props.onShowToast("error", "Failed to create salary data", error.message);
		} finally {
			this.props.onAddLoadingProgress();
		}
	};

	getWorks = async () => {
		try {
			this.props.onAddLoadingProgress(20);
			const response = await getWorks();
			this.onWorksChange(response.data);
		} catch (error) {
			this.onWorksChange([]);
			this.props.onShowToast("error", "Failed to get works data", error.message);
		} finally {
			this.props.onAddLoadingProgress();
		}
	};

	createWork = async () => {
		if (!this.state.selectedWork.name || this.state.selectedWork.name.trim() === "") {
			this.props.onShowToast("error", "Failed to add work data", "Work name cannot be empty.");
			return;
		}
		try {
			this.props.onAddLoadingProgress(20);
			const response = await createWork(this.state.selectedWork.name.trim());
			this.onHideCreateWorkModal();
			await getWorks();
			this.props.onShowToast("success", "Success!", response.message);
		} catch (error) {
			this.props.onShowToast("error", "Failed to create work data", error.message);
		} finally {
			this.props.onAddLoadingProgress();
		}
	};

	onPersonsChange = persons => {
		this.setState({
			...this.state,
			...{
				persons,
			},
		});
	};

	onSalariesChange = salaries => {
		this.setState({
			...this.state,
			...{
				salaries,
			},
		});
	};

	onWorksChange = works => {
		this.setState({
			...this.state,
			...{
				works,
			},
		});
	};

	onShowCreatePersonModal = () => {
		if (!this.props.readyState) {
			this.props.onShowToast(
				"warning",
				"Failed to add person data",
				"An operation is currently in progress. Please wait until current operation complete before switching data source.",
			);
			return;
		}
		if (!this.props.usingDynamicData) {
			this.props.onShowToast(
				"error",
				"Failed to add person data",
				"You are using static data. Please switch to dynamic data.",
			);
			return;
		}
		this.getWorks()
			.then(() => {
				return this.getSalaries();
			})
			.then(() => {
				this.setState({
					...this.state,
					...{
						selectedPerson: {
							name: "",
							id_salary: -1,
							id_work: -1,
						},
						createPersonModalShown: true,
					},
				});
			});
	};

	onHideCreatePersonModal = () => {
		this.setState({
			...this.state,
			...{
				selectedPerson: {},
				createPersonModalShown: false,
			},
		});
	};

	onShowModifyPersonModal = person => {
		if (!this.props.readyState) {
			this.props.onShowToast(
				"warning",
				"Failed to modify person data",
				"An operation is currently in progress. Please wait until current operation complete before switching data source.",
			);
			return;
		}
		if (!this.props.usingDynamicData) {
			this.props.onShowToast(
				"error",
				"Failed to modify person data",
				"You are using static data. Please switch to dynamic data.",
			);
			return;
		}
		this.getWorks()
			.then(() => {
				return this.getSalaries();
			})
			.then(() => {
				this.setState({
					...this.state,
					...{
						selectedPerson: person,
						modifyPersonModalShown: true,
					},
				});
			});
	};

	onHideModifyPersonModal = () => {
		this.setState({
			...this.state,
			...{
				selectedPerson: {},
				modifyPersonModalShown: false,
			},
		});
	};

	onShowDeletePersonModal = person => {
		if (!this.props.readyState) {
			this.props.onShowToast(
				"warning",
				"Failed to delete person data",
				"An operation is currently in progress. Please wait until current operation complete before switching data source.",
			);
			return;
		}
		if (!this.props.usingDynamicData) {
			this.props.onShowToast(
				"error",
				"Failed to delete person data",
				"You are using static data. Please switch to dynamic data.",
			);
			return;
		}
		this.setState({
			...this.state,
			...{
				selectedPerson: person,
				deletePersonModalShown: true,
			},
		});
	};

	onHideDeletePersonModal = () => {
		this.setState({
			...this.state,
			...{
				selectedPerson: {},
				deletePersonModalShown: false,
			},
		});
	};

	onShowCreateSalaryModal = () => {
		if (!this.props.readyState) {
			this.props.onShowToast(
				"warning",
				"Failed to add salary data",
				"An operation is currently in progress. Please wait until current operation complete before switching data source.",
			);
			return;
		}
		if (!this.props.usingDynamicData) {
			this.props.onShowToast(
				"error",
				"Failed to add salary data",
				"You are using static data. Please switch to dynamic data.",
			);
			return;
		}
		this.setState({
			...this.state,
			...{
				selectedSalary: {
					salary: 0,
				},
				createSalaryModalShown: true,
			},
		});
	};

	onHideCreateSalaryModal = () => {
		this.setState({
			...this.state,
			...{
				selectedSalary: {},
				createSalaryModalShown: false,
			},
		});
	};

	onShowCreateWorkModal = () => {
		if (!this.props.readyState) {
			this.props.onShowToast(
				"warning",
				"Failed to add work data",
				"An operation is currently in progress. Please wait until current operation complete before switching data source.",
			);
			return;
		}
		if (!this.props.usingDynamicData) {
			this.props.onShowToast(
				"error",
				"Failed to add work data",
				"You are using static data. Please switch to dynamic data.",
			);
			return;
		}
		this.setState({
			...this.state,
			...{
				selectedWork: {
					name: "",
				},
				createWorkModalShown: true,
			},
		});
	};

	onHideCreateWorkModal = () => {
		this.setState({
			...this.state,
			...{
				selectedWork: {},
				createWorkModalShown: false,
			},
		});
	};

	parseNumber = str => {
		const numberStrings = str.match(new RegExp(/\d+/g));
		if (numberStrings === null || numberStrings.length === 0) {
			return "";
		}
		return parseInt(numberStrings.join(""));
	};

	showNumberAsCurrency = number => {
		if (typeof number === "number") {
			number = number.toString();
		}
		number = number
			.split("")
			.reverse()
			.join("");
		const numberStrings = number.match(new RegExp(/.{1,3}/g));
		const currency = numberStrings
			.join(".")
			.split("")
			.reverse()
			.join("");
		return currency.concat(",00");
	};

	renderCurrency = number => {
		const currency = this.showNumberAsCurrency(this.parseNumber(number));
		return (
			<div style={{ clear: "both" }}>
				<span style={{ float: "left" }}>Rp&nbsp;</span>
				<span style={{ float: "right" }}>{currency}</span>
			</div>
		);
	};

	renderTableContent = () => {
		if (this.state.persons.length === 0) {
			return (
				<tr>
					<td colSpan="4">There is no person data available at the moment.</td>
				</tr>
			);
		}
		const tableRows = [];
		// eslint-disable-next-line
		for (const [index, person] of this.state.persons.entries()) {
			tableRows.push(
				<tr key={index}>
					<td>{person.name}</td>
					<td>{person.work}</td>
					<td>{this.renderCurrency(person.salary)}</td>
					<td>
						<ButtonToolbar aria-label="Action toolbar">
							<Button
								className="ml-auto"
								size="sm"
								variant="outline-info"
								onClick={() => {
									this.onShowModifyPersonModal(person);
								}}
							>
								Modify
							</Button>{" "}
							&nbsp;
							<Button
								className="mr-auto"
								size="sm"
								variant="outline-danger"
								onClick={() => {
									this.onShowDeletePersonModal(person);
								}}
							>
								Delete
							</Button>
						</ButtonToolbar>
					</td>
				</tr>,
			);
		}
		return tableRows;
	};

	renderSalaryOptions = () => {
		if (this.state.salaries.length === 0) {
			return (
				<option value={-1} disabled>
					No salary available
				</option>
			);
		}
		const options = [];
		options.push(
			<option key={0} value={-1} disabled>
				Please choose a salary
			</option>,
		);
		// eslint-disable-next-line
		for (const [index, salary] of this.state.salaries.entries()) {
			options.push(
				<option key={index + 1} value={salary.id}>
					Rp {this.showNumberAsCurrency(this.parseNumber(salary.salary))}
				</option>,
			);
		}
		return options;
	};

	renderWorkOptions = () => {
		if (this.state.works.length === 0) {
			return (
				<option value={-1} disabled>
					No work available
				</option>
			);
		}
		const options = [];
		options.push(
			<option key={0} value={-1} disabled>
				Please choose a work
			</option>,
		);
		// eslint-disable-next-line
		for (const [index, work] of this.state.works.entries()) {
			options.push(
				<option key={index + 1} value={work.id}>
					{work.name}
				</option>,
			);
		}
		return options;
	};

	render() {
		return (
			<Container className="dashboard" fluid>
				<Row className="row">
					<Col>
						<ButtonToolbar aria-label="Data toolbar">
							<Button variant="outline-warning" size="sm" onClick={this.onShowCreateSalaryModal}>
								Add Salary
							</Button>
							&nbsp;
							<Button variant="outline-warning" size="sm" onClick={this.onShowCreateWorkModal}>
								Add Work
							</Button>
							&nbsp;
							<Button
								className="ml-auto"
								variant="warning"
								size="sm"
								onClick={this.onShowCreatePersonModal}
							>
								Add Person Data
							</Button>
						</ButtonToolbar>
					</Col>
				</Row>
				<Row className="row">
					<Col>
						<Table responsive bordered hover>
							<thead>
								<tr>
									<th>Name</th>
									<th>Work</th>
									<th>Salary</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>{this.renderTableContent()}</tbody>
						</Table>
					</Col>
				</Row>
				{/* Person: Create */}
				<Modal show={this.state.createPersonModalShown} onHide={this.onHideCreatePersonModal}>
					<Modal.Header closeButton>
						<Modal.Title>Add Person Data</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form>
							<Form.Group as={Row}>
								<Form.Label column sm="2">
									Name
								</Form.Label>
								<Col sm="10">
									<Form.Control
										disabled={!this.props.readyState}
										onChange={({ target }) => {
											this.setState({
												...this.state,
												...{
													selectedPerson: {
														...this.state.selectedPerson,
														...{ name: target.value },
													},
												},
											});
										}}
										placeholder="Enter person name"
									/>
								</Col>
							</Form.Group>
							<Form.Group as={Row}>
								<Form.Label column sm="2">
									Work
								</Form.Label>
								<Col sm="10">
									<Form.Control
										as="select"
										disabled={!this.props.readyState}
										onChange={({ target }) => {
											this.setState({
												...this.state,
												...{
													selectedPerson: {
														...this.state.selectedPerson,
														...{ id_work: target.value },
													},
												},
											});
										}}
										defaultValue={this.state.selectedPerson.id_work}
									>
										{this.renderWorkOptions()}
									</Form.Control>
								</Col>
							</Form.Group>
							<Form.Group as={Row}>
								<Form.Label column sm="2">
									Salary
								</Form.Label>
								<Col sm="10">
									<Form.Control
										as="select"
										disabled={!this.props.readyState}
										onChange={({ target }) => {
											this.setState({
												...this.state,
												...{
													selectedPerson: {
														...this.state.selectedPerson,
														...{ id_salary: target.value },
													},
												},
											});
										}}
										defaultValue={this.state.selectedPerson.id_salary}
									>
										{this.renderSalaryOptions()}
									</Form.Control>
								</Col>
							</Form.Group>
						</Form>
					</Modal.Body>
					<Modal.Footer>
						<Button
							variant="secondary"
							onClick={this.onHideCreatePersonModal}
							disabled={!this.props.readyState}
						>
							Cancel
						</Button>
						<Button variant="primary" onClick={this.createPerson} disabled={!this.props.readyState}>
							Add Data
						</Button>
					</Modal.Footer>
				</Modal>
				{/* Person: Modify */}
				<Modal show={this.state.modifyPersonModalShown} onHide={this.onHideModifyPersonModal}>
					<Modal.Header closeButton>
						<Modal.Title>Modify Person Data</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form.Group as={Row}>
							<Form.Label column sm="2">
								Name
							</Form.Label>
							<Col sm="10">
								<Form.Control
									disabled={!this.props.readyState}
									onChange={({ target }) => {
										this.setState({
											...this.state,
											...{
												selectedPerson: {
													...this.state.selectedPerson,
													...{ name: target.value },
												},
											},
										});
									}}
									placeholder="Enter person name"
									defaultValue={this.state.selectedPerson.name}
								/>
							</Col>
						</Form.Group>
						<Form.Group as={Row}>
							<Form.Label column sm="2">
								Work
							</Form.Label>
							<Col sm="10">
								<Form.Control
									as="select"
									disabled={!this.props.readyState}
									onChange={({ target }) => {
										this.setState({
											...this.state,
											...{
												selectedPerson: {
													...this.state.selectedPerson,
													...{ id_work: target.value },
												},
											},
										});
									}}
									defaultValue={this.state.selectedPerson.id_work}
								>
									{this.renderWorkOptions()}
								</Form.Control>
							</Col>
						</Form.Group>
						<Form.Group as={Row}>
							<Form.Label column sm="2">
								Salary
							</Form.Label>
							<Col sm="10">
								<Form.Control
									as="select"
									disabled={!this.props.readyState}
									onChange={({ target }) => {
										this.setState({
											...this.state,
											...{
												selectedPerson: {
													...this.state.selectedPerson,
													...{ id_salary: target.value },
												},
											},
										});
									}}
									defaultValue={this.state.selectedPerson.id_salary}
								>
									{this.renderSalaryOptions()}
								</Form.Control>
							</Col>
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button
							variant="secondary"
							onClick={this.onHideModifyPersonModal}
							disabled={!this.props.readyState}
						>
							Cancel
						</Button>
						<Button variant="primary" onClick={this.modifyPerson} disabled={!this.props.readyState}>
							Save Changes{" "}
						</Button>
					</Modal.Footer>
				</Modal>
				{/* Person: Delete */}
				<Modal show={this.state.deletePersonModalShown} onHide={this.onHideDeletePersonModal}>
					<Modal.Header closeButton>
						<Modal.Title>Delete Person Data</Modal.Title>
					</Modal.Header>
					<Modal.Body>Are you sure you would like to delete {this.state.selectedPerson.name}?</Modal.Body>
					<Modal.Footer>
						<Button
							variant="secondary"
							onClick={this.onHideDeletePersonModal}
							disabled={!this.props.readyState}
						>
							Cancel
						</Button>
						<Button variant="danger" onClick={this.deletePerson} disabled={!this.props.readyState}>
							Delete {this.state.selectedPerson.name}
						</Button>
					</Modal.Footer>
				</Modal>
				{/* Salary: Create */}
				<Modal show={this.state.createSalaryModalShown} onHide={this.onHideCreateSalaryModal}>
					<Modal.Header closeButton>
						<Modal.Title>Add Salary Data</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form>
							<Form.Group as={Row}>
								<Form.Label column sm="2">
									Salary
								</Form.Label>
								<Col sm="10">
									<Form.Control
										disabled={!this.props.readyState}
										onChange={({ target }) => {
											target.value = this.parseNumber(target.value.trim());
											this.setState({
												...this.state,
												...{
													selectedSalary: {
														...this.state.selectedSalary,
														...{ salary: target.value },
													},
												},
											});
										}}
										placeholder="Enter salary"
									/>
								</Col>
							</Form.Group>
						</Form>
					</Modal.Body>
					<Modal.Footer>
						<Button
							variant="secondary"
							onClick={this.onHideCreateSalaryModal}
							disabled={!this.props.readyState}
						>
							Cancel
						</Button>
						<Button variant="primary" onClick={this.createSalary} disabled={!this.props.readyState}>
							Add Data
						</Button>
					</Modal.Footer>
				</Modal>
				{/* Work: Create */}
				<Modal show={this.state.createWorkModalShown} onHide={this.onHideCreateWorkModal}>
					<Modal.Header closeButton>
						<Modal.Title>Add Work Data</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form>
							<Form.Group as={Row}>
								<Form.Label column sm="2">
									Name
								</Form.Label>
								<Col sm="10">
									<Form.Control
										disabled={!this.props.readyState}
										onChange={({ target }) => {
											this.setState({
												...this.state,
												...{
													selectedWork: {
														...this.state.selectedWork,
														...{ name: target.value },
													},
												},
											});
										}}
										placeholder="Enter work name"
									/>
								</Col>
							</Form.Group>
						</Form>
					</Modal.Body>
					<Modal.Footer>
						<Button
							variant="secondary"
							onClick={this.onHideCreateWorkModal}
							disabled={!this.props.readyState}
						>
							Cancel
						</Button>
						<Button variant="primary" onClick={this.createWork} disabled={!this.props.readyState}>
							Add Data
						</Button>
					</Modal.Footer>
				</Modal>
			</Container>
		);
	}
}
