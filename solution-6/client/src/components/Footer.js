/**
 * Solution of Problem 6
 * Date     : September 14, 2019
 * Problem  : Create a project.
 *
 * This program was written and submitted for Bootcamp Arkademy
 * Batch 12 selection.
 *
 * Copyright 2019, Danang Galuh Tegar Prasetyo.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Logo from "./../assets/logo.png";
import "./Footer.css";

export default function Footer() {
	return (
		<footer className="footer border-top">
			<Row>
				<Col>
					<img className="mb-2" src={Logo} alt="" width="24" height="24" />
					<small className="d-block mb-3 text-muted copyright">
						<strong>Submission for Bootcamp Arkademy Batch 12 Selection</strong>
						<br />
						Copyright &copy; {new Date().getFullYear()} Danang Galuh Tegar Prasetyo. All right reserved.
						<br />
						This program is available in form of source code and is licensed by Apache-2.0 license.
					</small>
				</Col>
			</Row>
		</footer>
	);
}
