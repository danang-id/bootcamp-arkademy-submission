/**
 * Solution of Problem 6
 * Date     : September 14, 2019
 * Problem  : Create a project.
 *
 * This program was written and submitted for Bootcamp Arkademy
 * Batch 12 selection.
 *
 * Copyright 2019, Danang Galuh Tegar Prasetyo.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from "react";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Button from "react-bootstrap/Button";
import Logo from "./../assets/arkademy.png";

export default function NavigationBar(props) {
	return (
		<Navbar collapseOnSelect expand="lg" bg="light" variant="light">
			<Navbar.Brand href="#">
				<img alt="Arkademy" src={Logo} width="70px" />
				&nbsp; Bootcamp Arkademy
			</Navbar.Brand>
			<Navbar.Toggle aria-controls="responsive-navbar-nav" />
			<Navbar.Collapse id="responsive-navbar-nav">
				<Nav>
					<Nav.Link href="#">Solution to Problem 6</Nav.Link>
				</Nav>
				<Nav>
					<Nav.Link href="https://gitlab.com/danang-id/bootcamp-arkademy-submission" target="_blank">
						Source Code
					</Nav.Link>
				</Nav>
				<Nav className="ml-auto">
					<ButtonGroup aria-label="Basic example">
						<Button
							variant={props.usingDynamicData ? "outline-info" : "info"}
							onClick={() => {
								props.onChangeDataSource(false);
							}}
						>
							Static Data
						</Button>
						<Button
							variant={props.usingDynamicData ? "info" : "outline-info"}
							onClick={() => {
								props.onChangeDataSource(true);
							}}
						>
							Dynamic Data
						</Button>
					</ButtonGroup>
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	);
}
