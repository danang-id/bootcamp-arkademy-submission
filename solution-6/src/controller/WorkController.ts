/**
 * Solution of Problem 6
 * Date     : September 14, 2019
 * Problem  : Create a project.
 *
 * This program was written and submitted for Bootcamp Arkademy
 * Batch 12 selection.
 *
 * Copyright 2019, Danang Galuh Tegar Prasetyo.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Request, Response } from "express-serve-static-core";
import { initialiseDatabase } from "../helpers/database";
import { validateRequest, RequestRequirements } from "../helpers/express";
import { IWork } from "../model/IWork";

export async function getWorks(request: Request, response: Response) {
	let database;
	try {
		database = await initialiseDatabase();
		const works: IWork[] = await database.all("SELECT * FROM Work");
		response.json({
			success: true,
			code: 200,
			data: works,
		});
	} catch (error) {
		throw error;
	} finally {
		if (typeof database !== "undefined") {
			await database.close();
		}
	}
}

export async function createWork(request: Request, response: Response) {
	let database;
	try {
		const requirements: RequestRequirements = {
			body: ["name"],
		};
		validateRequest(request, requirements);
		database = await initialiseDatabase();
		const result = await database.run("INSERT INTO Work (name) VALUES (?)", [request.body.name]);
		const work: IWork = await database.get("SELECT * FROM Work WHERE id = ?", [result.lastID]);
		response.json({
			success: true,
			code: 200,
			message: "Successfully added work " + work.name + "!",
			data: work,
		});
	} catch (error) {
		throw error;
	} finally {
		if (typeof database !== "undefined") {
			await database.close();
		}
	}
}
