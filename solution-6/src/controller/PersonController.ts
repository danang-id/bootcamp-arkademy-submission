/**
 * Solution of Problem 6
 * Date     : September 14, 2019
 * Problem  : Create a project.
 *
 * This program was written and submitted for Bootcamp Arkademy
 * Batch 12 selection.
 *
 * Copyright 2019, Danang Galuh Tegar Prasetyo.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import { Request, Response } from "express-serve-static-core";

import { initialiseDatabase } from "../helpers/database";
import { validateRequest, RequestRequirements } from "../helpers/express";
import { IPerson } from "../model/IPerson";
import { ICategory } from "../model/ICategory";
import { IWork } from "../model/IWork";

export async function getPersons(request: Request, response: Response) {
	let database;
	try {
		database = await initialiseDatabase();
		const persons: IPerson[] = await database.all(
			"SELECT Person.id, Person.name, Person.id_work, Person.id_salary, Category.salary, Work.name AS work " +
			"FROM Person " +
			"INNER JOIN Category ON Category.id = Person.id_salary " +
			"INNER JOIN Work ON Work.id = Person.id_work"
		);
		response.json({
			success: true,
			code: 200,
			data: persons,
		});
	} catch (error) {
		throw error;
	} finally {
		if (typeof database !== "undefined") {
			await database.close();
		}
	}
}

export async function createPerson(request: Request, response: Response) {
	let database;
	try {
		database = await initialiseDatabase();
		const requirements: RequestRequirements = {
			body: ["name", "id_salary", "id_work"],
		};
		validateRequest(request, requirements);
		const salary: ICategory = await database.get("SELECT * FROM Category WHERE id = ?", [request.body.id_salary]);
		if (typeof salary === "undefined") {
			response.json({
				success: false,
				code: 400,
				message: "Salary with ID " + request.body.id_salary + " is not found.",
			});
			return;
		}
		const work: IWork = await database.get("SELECT * FROM Work WHERE id = ?", [request.body.id_work]);
		if (typeof work === "undefined") {
			response.json({
				success: false,
				code: 400,
				message: "Work with ID " + request.body.id_work + " is not found.",
			});
			return;
		}
		const result = await database.run("INSERT INTO Person (name, id_salary, id_work) VALUES (?,?,?)", [
			request.body.name,
			request.body.id_salary,
			request.body.id_work,
		]);
		const person: IPerson = await database.get(
			"SELECT Person.id, Person.name, Person.id_work, Person.id_salary, Category.salary, Work.name AS work " +
			"FROM Person " +
			"INNER JOIN Category ON Category.id = Person.id_salary " +
			"INNER JOIN Work ON Work.id = Person.id_work " +
			"WHERE Person.id = ?",
			[result.lastID]
		);
		response.json({
			success: true,
			code: 200,
			message:
				"Successfully added " +
				person.name +
				" as a " +
				work.name +
				" with salary of Rp " +
				salary.salary +
				".",
			data: person
		});
	} catch (error) {
		throw error;
	} finally {
		if (typeof database !== "undefined") {
			await database.close();
		}
	}
}

export async function modifyPerson(request: Request, response: Response) {
	let database;
	try {
		database = await initialiseDatabase();
		const requirements: RequestRequirements = {
			body: ["name"],
			query: ["id"],
		};
		validateRequest(request, requirements);
		let person: IPerson = await database.get("SELECT * FROM Person WHERE id = ?", [request.query.id]);
		if (typeof person === "undefined") {
			response.json({
				success: false,
				code: 400,
				message: "Person with ID " + request.query.id + " is not found.",
			});
			return;
		}
		if (!!request.body.id_salary) {
			const salary: ICategory = await database.get("SELECT * FROM Category WHERE id = ?", [request.body.id_salary]);
			if (typeof salary === "undefined") {
				response.json({
					success: false,
					code: 400,
					message: "Salary with ID " + request.body.id_salary + " is not found.",
				});
				return;
			}
		}
		if (!!request.body.id_work) {
			const work: IWork = await database.get("SELECT * FROM Work WHERE id = ?", [request.body.id_work]);
			if (typeof work === "undefined") {
				response.json({
					success: false,
					code: 400,
					message: "Work with ID " + request.body.id_work + " is not found.",
				});
				return;
			}
		}
		person.name = request.body.name || person.name;
		person.id_salary = request.body.id_salary || person.id_salary;
		person.id_work = request.body.id_work || person.id_work;
		await database.run("UPDATE Person SET name = ?, id_salary = ?, id_work = ? WHERE id = ? ", [
			person.name,
			person.id_salary,
			person.id_work,
			person.id,
		]);
		person = await database.get(
			"SELECT Person.id, Person.name, Person.id_work, Person.id_salary, Category.salary, Work.name AS work " +
			"FROM Person " +
			"INNER JOIN Category ON Category.id = Person.id_salary " +
			"INNER JOIN Work ON Work.id = Person.id_work " +
			"WHERE Person.id = ?",
			[person.id]
		);
		response.json({
			success: true,
			code: 200,
			message: "Successfully modified " + person.name + ".",
			data: person
		});
	} catch (error) {
		throw error;
	} finally {
		if (typeof database !== "undefined") {
			await database.close();
		}
	}
}

export async function deletePerson(request: Request, response: Response) {
	let database;
	try {
		database = await initialiseDatabase();
		const requirements: RequestRequirements = {
			query: ["id"],
		};
		validateRequest(request, requirements);
		let person: IPerson = await database.get("SELECT * FROM Person WHERE id = ?", [request.query.id]);
		if (typeof person === "undefined") {
			response.json({
				success: false,
				code: 400,
				message: "Person with ID " + request.query.id + " is not found.",
			});
			return;
		}
		await database.run("DELETE FROM Person WHERE id = ?", [person.id]);
		response.json({
			success: true,
			code: 200,
			message: "Successfully deleted " + person.name + "."
		});
	} catch (error) {
		throw error;
	} finally {
		if (typeof database !== "undefined") {
			await database.close();
		}
	}
}
