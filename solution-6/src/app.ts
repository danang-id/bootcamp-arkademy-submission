/**
 * Solution of Problem 6
 * Date     : September 14, 2019
 * Problem  : Create a project.
 *
 * This program was written and submitted for Bootcamp Arkademy
 * Batch 12 selection.
 *
 * Copyright 2019, Danang Galuh Tegar Prasetyo.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import "@babel/polyfill";
import http from "http";
import path from "path";
import express from "express";
import cors from "cors";
import { json, urlencoded } from "body-parser";

import Controllers from "./controller";
import Services from "./services";
import { createErrorHandler, createRouter, favicon, showLog } from "./helpers/express";
import { createLogger } from "./helpers/logger";

const app = express();
const router = createRouter(app);
const logger = createLogger("arkademy:Application");
const server = new http.Server(app);
const port = process.env.PORT || 9000;

app.use(json());
app.use(urlencoded({ extended: false }));
app.use(cors());
app.use(showLog(logger));
app.use(favicon(path.join(__dirname, "..", "client", "build", "favicon.ico")));
app.use(express.static(path.join(__dirname, "..", "client", "build")));

for (const key in Services) {
	if (Services.hasOwnProperty(key)) {
		const service = Services[key];
		logger.i("Added " + key + " service on endpoint " + service.method + " " + service.endpoint + ".");
		(<{ [k: string]: any }>router)[service.method.toLowerCase()](service.endpoint)
			.handle(Controllers[service.handler]);
	}
}

app.use(Controllers.serve);
app.use(createErrorHandler(logger));

server.listen(port, function () {
	logger.i("Server listening http://localhost:" + port + ".");
});
