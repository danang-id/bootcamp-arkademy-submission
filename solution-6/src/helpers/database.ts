/**
 * Solution of Problem 6
 * Date     : September 14, 2019
 * Problem  : Create a project.
 *
 * This program was written and submitted for Bootcamp Arkademy
 * Batch 12 selection.
 *
 * Copyright 2019, Danang Galuh Tegar Prasetyo.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import fs from "fs";
import sqlite, { Database } from "sqlite";
import DBConfig from "../config/database.config";

export async function initialiseDatabase(): Promise<Database> {
	try {
		const dbPath: string = DBConfig.options.dbPath.toString();
		if (!fs.existsSync(dbPath)) {
			fs.writeFileSync(dbPath, "");
			const database = await sqlite.open(dbPath, { cached: false });
			await database.run("CREATE TABLE Person (id INTEGER PRIMARY KEY, name VARCHAR(255), id_salary INTEGER, id_work INTEGER)");
			await database.run("CREATE TABLE Category (id INTEGER PRIMARY KEY, salary VARCHAR(255))");
			await database.run("CREATE TABLE Work (id INTEGER PRIMARY KEY, name VARCHAR(255))");
			await database.close();
		}
		return await sqlite.open(dbPath, { cached: false });
	} catch (error) {
		throw error;
	}
}
