/**
 * Solution of Problem 6
 * Date     : September 14, 2019
 * Problem  : Create a project.
 *
 * This program was written and submitted for Bootcamp Arkademy
 * Batch 12 selection.
 *
 * Copyright 2019, Danang Galuh Tegar Prasetyo.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { HTTP_METHOD } from "./helpers/express";

function endpoint(uri: string) {
	return "/api/v1/".concat(uri);
}

export const Services: ServicesType = {
	GetPersons: {
		endpoint: endpoint("persons"),
		method: HTTP_METHOD.GET,
		handler: "getPersons"
	},
	CreatePerson: {
		endpoint: endpoint("person"),
		method: HTTP_METHOD.POST,
		handler: "createPerson"
	},
	ModifyPerson: {
		endpoint: endpoint("person"),
		method: HTTP_METHOD.PUT,
		handler: "modifyPerson"
	},
	DeletePerson: {
		endpoint: endpoint("person"),
		method: HTTP_METHOD.DELETE,
		handler: "deletePerson"
	},
	GetSalaries: {
		endpoint: endpoint("salaries"),
		method: HTTP_METHOD.GET,
		handler: "getSalaries"
	},
	CreateSalary: {
		endpoint: endpoint("salary"),
		method: HTTP_METHOD.POST,
		handler: "createSalary"
	},
	GetWorks: {
		endpoint: endpoint("works"),
		method: HTTP_METHOD.GET,
		handler: "getWorks"
	},
	CreateWork: {
		endpoint: endpoint("work"),
		method: HTTP_METHOD.POST,
		handler: "createWork"
	},
};

export default Services

type ServicesType = {
	[k: string]: {
		endpoint: string,
		method: HTTP_METHOD,
		handler: string
	}
}
