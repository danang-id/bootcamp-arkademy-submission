module.exports = {
	apps: [
		{
			name: 'bootcamp-arkademy-submission',
			script: 'lib/app.js',
			instances: 'max',
			autorestart: true,
			watch: false,
			max_memory_restart: '1G',
			env: {
				NODE_ENV: 'development',
				DEBUG: 'arkademy:*'
			},
			env_production: {
				NODE_ENV: 'production',
				DEBUG: 'arkademy:*'
			}
		}
	]
};
